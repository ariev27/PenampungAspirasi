package id.dangersstudio.penampungaspirasi.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

import id.dangersstudio.penampungaspirasi.R;
import id.dangersstudio.penampungaspirasi.activity.LihatResponActivity;
import id.dangersstudio.penampungaspirasi.helpers.Constants;
import id.dangersstudio.penampungaspirasi.models.ModelPengaduanSaya;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by ariev27 on 8/24/17.
 */

public class PengaduanSayaAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<ModelPengaduanSaya> pengaduanSaya;
    private Context c;

    public PengaduanSayaAdapter(Context c, ArrayList<ModelPengaduanSaya> pengaduanSaya) {
        this.pengaduanSaya = pengaduanSaya;
        this.c = c;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(c).inflate(R.layout.list_pengaduan_saya, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final ModelPengaduanSaya x = pengaduanSaya.get(position);
        MyViewHolder mholder = (MyViewHolder) holder;
        mholder.tvTglPengaduan.setText(x.getTgl_pengaduan());
        mholder.tvPengaduan.setText(x.getPengaduan());

        String keluhan = "Keluhan Pelayanan";
        String laporan = "Laporan Keterjadian";

        if (x.getId_kategori().equals("1")) {
            mholder.tvKategoriPengaduan.setText(keluhan);
        } else {
            mholder.tvKategoriPengaduan.setText(laporan);
        }

        Glide
                .with(c)
                .load(x.getFoto())
                .diskCacheStrategy(DiskCacheStrategy.RESULT)
                .bitmapTransform(new CropCircleTransformation(c))
                //.centerCrop()
                .placeholder(R.drawable.ic_profile_default)
                .crossFade()
                .into(mholder.ivPengaduan);

        mholder.containerPengaduan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(c, LihatResponActivity.class);
                i.putExtra(Constants.ID_PENGADUAN, x.getId_pengaduan());
                i.putExtra(Constants.ID_KATEGORI, x.getId_kategori());
                i.putExtra(Constants.PENGADUAN, x.getPengaduan());
                i.putExtra(Constants.TGL_PENGADUAN, x.getTgl_pengaduan());
                i.putExtra(Constants.FOTO_PENGADUAN, x.getFoto());
                c.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return pengaduanSaya == null ? 0 : pengaduanSaya.size();
    }

    //sisipkan view holder
    private class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView ivPengaduan;
        TextView tvTglPengaduan, tvKategoriPengaduan, tvPengaduan;
        RelativeLayout containerPengaduan;

        MyViewHolder(View v) {
            super(v);
            ivPengaduan = v.findViewById(R.id.ivPengaduan);
            tvTglPengaduan = v.findViewById(R.id.tvTglPengaduan);
            tvKategoriPengaduan = v.findViewById(R.id.tvKategoriPengaduan);
            tvPengaduan = v.findViewById(R.id.tvPengaduan);
            containerPengaduan = v.findViewById(R.id.containerPengaduan);
        }
    }
}