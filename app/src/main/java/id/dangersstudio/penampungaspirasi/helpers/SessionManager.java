package id.dangersstudio.penampungaspirasi.helpers;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import id.dangersstudio.penampungaspirasi.activity.LoginActivity;


public class SessionManager {
    // Shared Preferences
    SharedPreferences pref;
    // Editor for Shared preferences
    Editor editor;
    // Contjava.lang.Stringext
    Context _context;
    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "PenampungAspirasi";
    private static final String IS_LOGIN = "isLoggedIn";
    public static final String KEY_NAME = "nama", UPDATE_PASSWORD = "updatePassword";
    public static final String KEY_TOKEN = "token",  LEVEL = "level", IDUser = "IdUser", USERNAME = "username", PASSWORD = "password",
            NAMA_LENGKAP = "nama_lengkap", ALAMAT = "alamat", NO_TELP = "no_telp", FOTO = "foto";
    private static final String KEY_SESSION = "sessionID", IS_SESSION = "isSession", TIPE_LOGIN = "tipeLogin";
    //private static final String INTRO = "intro Applikasi";

    // Constructor
    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void createLoginSession(String token) {
        // Storing login value as TRUE
        editor.putString(KEY_TOKEN, getToken());
        editor.putBoolean(IS_LOGIN, true);
        // commit changes
        editor.commit();
    }

    public void createLoginSession() {
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);
        // commit changes
        editor.commit();
    }

    /**
     * Check login method wil check user login status If false it will redirect
     * user to login page Else won't do anything
     */
    public void checkLogin() {
        // Check login status
        if (!this.isLoggedIn()) {
            // Clearing all data from Shared Preferences
            editor.clear();
            editor.commit();
            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(_context, LoginActivity.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
            _context.startActivity(i);
        }

    }


    /**
     * Clear session details
     * public void logoutUser() {
     * // Clearing all data from Shared Preferences
     * editor.clear();
     * editor.commit();
     * <p>
     * <p>
     * // After logout redirect user to Loing Activity
     * Intent i = new Intent(_context, LoginActivity.class);
     * // Closing all the Activities
     * i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
     * <p>
     * // Add new Flag to start new Activity
     * i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
     * <p>
     * // Staring Login Activity
     * _context.startActivity(i);
     * <p>
     * }
     */

    public void logoutUser2() {
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();

    }

    // Getter & Setter untuk mengambil data session
    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }

    public Boolean getIsSession() {
        return pref.getBoolean(IS_SESSION, false);
    }

    public void setIsSession(boolean v) {
        editor.putBoolean(IS_SESSION, v);
        editor.commit();
    }

    public String getSession() {
        return pref.getString(KEY_SESSION, "");
    }

    public void setSession(String v) {
        editor.putString(KEY_SESSION, v);
        editor.commit();
    }

    public String getToken() {
        return pref.getString(KEY_TOKEN, "");
    }

    public void setToken(String v) {
        editor.putString(KEY_TOKEN, v);
        editor.commit();
    }

    public String getToken2() {
        if (getIsSession()) {
            return pref.getString(KEY_SESSION, "");
        } else {
            return pref.getString(KEY_TOKEN, "");
        }
    }

    public String getIDUser() {
        return pref.getString(IDUser, "");
    }

    public void setIDUser(String v) {
        editor.putString(IDUser, v);
        editor.commit();
    }

    public String getLevel() {
        return pref.getString(LEVEL, "");
    }

    public void setLevel(String v) {
        editor.putString(LEVEL, v);
        editor.commit();
    }

    public String getUsername() {
        return pref.getString(USERNAME, "");
    }

    public void setUsername(String v) {
        editor.putString(USERNAME, v);
        editor.commit();
    }

    public String getPassword() {
        return pref.getString(PASSWORD, "");
    }

    public void setPassword(String v) {
        editor.putString(PASSWORD, v);
        editor.commit();
    }

    public String getNamaLengkap() {
        return pref.getString(NAMA_LENGKAP, "");
    }

    public void setNamaLengkap(String v) {
        editor.putString(NAMA_LENGKAP, v);
        editor.commit();
    }

    public String getAlamat() {
        return pref.getString(ALAMAT, "");
    }

    public void setAlamat(String v) {
        editor.putString(ALAMAT, v);
        editor.commit();
    }

    public String getNotelp() {
        return pref.getString(NO_TELP, "");
    }

    public void setNotelp(String v) {
        editor.putString(NO_TELP, v);
        editor.commit();
    }

    public String getFoto() {
        return pref.getString(FOTO, "");
    }

    public void setFoto(String v) {
        editor.putString(FOTO, v);
        editor.commit();
    }

//
//	public String get() {
//		return pref.getString("", "");
//	}
//
//	public void set(String v) {
//		editor.putString("", v);
//		editor.commit();
//	}

}
