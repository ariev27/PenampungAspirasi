package id.dangersstudio.penampungaspirasi.helpers;

/**
 * Created by ariev27 on 8/26/17.
 */

public class Constants {
    public static final String ID_PENGADUAN = "idPengaduan";
    public static final String ID_USER = "idUser";
    public static final String ID_KATEGORI = "idKategori";
    public static final String PENGADUAN = "pengaduan";
    public static final String TGL_PENGADUAN = "tglPengaduan";
    public static final String FOTO_PENGADUAN = "fotoPengaduan";
    public static final String FOTO_RESPON = "fotoRespon";
}
