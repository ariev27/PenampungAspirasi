package id.dangersstudio.penampungaspirasi.base;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.AlphaAnimation;
import android.widget.TextView;

import java.util.concurrent.TimeUnit;

import id.dangersstudio.penampungaspirasi.activity.LoginActivity;
import id.dangersstudio.penampungaspirasi.helpers.SessionManager;
import okhttp3.OkHttpClient;


/**
 * Created by Ariev27 on 24/01/2016.
 */
public class BaseActivity extends AppCompatActivity {
    protected Context c;
    protected SessionManager sesi;
    protected AlphaAnimation btnAnimasi = new AlphaAnimation(1F, 0.7F);

    protected TextView tvTitle;
    protected OkHttpClient client;
    protected ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


        c = this;
        sesi = new SessionManager(c);
        // sesi.checkLogin();
        dialog = new ProgressDialog(c);

        client = new OkHttpClient.Builder()
                .connectTimeout(300, TimeUnit.SECONDS)
                .writeTimeout(300, TimeUnit.SECONDS)
                .readTimeout(300, TimeUnit.SECONDS)
                .build();

    }

    public void showLoading() {

        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        dialog.setInverseBackgroundForced(false);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setMessage("Loading...");

        dialog.show();
    }

    public void hideLoading() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    protected void showHeader() {
        ActionBar actionBar = getSupportActionBar();
        //actionBar.setLogo(R.drawable.logo_login);
        actionBar.setDisplayUseLogoEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false); //optional
        actionBar.setDisplayShowTitleEnabled(false); //optional
    }

    public void dialogKeluar() {
        AlertDialog.Builder dialogz = new AlertDialog.Builder(
                c);
        dialogz.setMessage("Anda yakin mau keluar ?");
        dialogz.setTitle("Konfirmasi");
        dialogz.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                sesi.logoutUser2();


                Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                // Closing all the Activities
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                // Add new Flag to start new Activity
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();


            }
        });
        dialogz.setNegativeButton("Tidak",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogx, int id) {
                        dialogx.cancel();
                    }
                });
        dialogz.show();
    }

    /*
    protected void drawerMenu(int posisi) {

        Toolbar toolbar = (Toolbar) this.findViewById(R.id.toolbar);
        this.setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //tvTitle = (TextView) findViewById(R.id.tvJudul);


        SecondaryDrawerItem home = new SecondaryDrawerItem().withName("Home")
                .withIcon(GoogleMaterial.Icon.gmd_home).withTag("home");

        SecondaryDrawerItem peta = new SecondaryDrawerItem().withName("Peta Lokasi")
                .withIcon(GoogleMaterial.Icon.gmd_map).withTag("peta");

        SecondaryDrawerItem kategori = new SecondaryDrawerItem().withName("Kategori")
                .withIcon(GoogleMaterial.Icon.gmd_local_offer).withTag("kategori");

        SecondaryDrawerItem jenis = new SecondaryDrawerItem().withName("Jenis")
                .withIcon(GoogleMaterial.Icon.gmd_photo).withTag("jenis");

        SecondaryDrawerItem lokasiSaya = new SecondaryDrawerItem().withName("Lokasi Saya")
                .withIcon(GoogleMaterial.Icon.gmd_location_on).withTag("lokasiSaya");

        SecondaryDrawerItem logout = new SecondaryDrawerItem().withName("Logout")
                .withIcon(GoogleMaterial.Icon.gmd_input).withTag("logout");


        SecondaryDrawerItem broadcast = new SecondaryDrawerItem().withName("Broadcast")
                .withIcon(GoogleMaterial.Icon.gmd_record_voice_over).withTag("broadcast");

        SecondaryDrawerItem alert = new SecondaryDrawerItem().withName("Alert")
                .withIcon(GoogleMaterial.Icon.gmd_speaker_phone).withTag("alert");

        SecondaryDrawerItem pesan = new SecondaryDrawerItem().withName("Pesan")
                .withIcon(GoogleMaterial.Icon.gmd_chat).withTag("pesan");

        SecondaryDrawerItem setting = new SecondaryDrawerItem().withName("Setting")
                .withIcon(GoogleMaterial.Icon.gmd_settings).withTag("setting");

        SecondaryDrawerItem about = new SecondaryDrawerItem().withName("About Us")
                .withIcon(GoogleMaterial.Icon.gmd_contacts).withTag("about");

        byte[] decodedString = Base64.decode(sesi.getAvatar(), Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.color.drawer_header)
                .addProfiles(
                        new ProfileDrawerItem().withName(sesi.getNama())
                                .withEmail(sesi.getNamaLevel() + " - " + sesi.getEmail())
                                .withIcon(decodedByte)
                )
                .withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
                    @Override
                    public boolean onProfileChanged(View view, IProfile profile, boolean currentProfile) {
                        return false;
                    }
                })
                .build();


        //create the drawer and remember the `Drawer` result object
        result = new DrawerBuilder();

        result.withAccountHeader(headerResult)
                .withActivity(this)
                .withToolbar(toolbar);

        if (sesi.getIdLevel().equalsIgnoreCase("penyuluh") || sesi.getIdLevel().equalsIgnoreCase("verifikator")) {
            result.addDrawerItems(
                    home,
                    peta,
                    kategori,
                    jenis,
                    lokasiSaya,
                    broadcast,
                    alert,
                    pesan,
                    new SectionDrawerItem().withName("Account"),
                    setting,
                    logout
            ).withSelectedItemByPosition(posisi).build();
        } else {
            result.addDrawerItems(
                    home,
                    peta,
                    kategori,
                    jenis,
                    lokasiSaya,
                    broadcast,
                    alert,
                    pesan,
                    new SectionDrawerItem().withName("Account"),
                    setting,
                    logout
            ).withSelectedItemByPosition(posisi).build();
        }


        result.withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
            @Override
            public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                Intent i = null;

                String judul = (String) drawerItem.getTag();

                switch (judul) {
                    case "home":
                        i = new Intent(c, MainActivity.class);
                        startActivity(i);
                        finish();
                        break;

                    case "peta":
                        i = new Intent(c, PetaLokasiActivity.class);
                        startActivity(i);
                        finish();
                        break;

                    case "kategori":
                        i = new Intent(c, KategoriActivity.class);
                        startActivity(i);
                        finish();
                        break;

                    case "jenis":
                        i = new Intent(c, JenisActivity.class);
                        startActivity(i);
                        finish();
                        break;

                    case "lokasiSaya":
                        i = new Intent(c, LokasiSayaActivity.class);
                        startActivity(i);
                        finish();
                        break;

                    case "broadcast":
                        //  i = new Intent(c, BroadcastActivity.class);
                        //startActivity(i);
                        //finish();
                        break;

                    case "pesan":
                        //i = new Intent(c, PesanActivity.class);
                        //startActivity(i);
                        //finish();
                        break;

                    case "alert":
                        //i = new Intent(c, AlertActivity.class);
                        //startActivity(i);
                        //finish();
                        break;

                    case "setting":
                        //i = new Intent(c, SettingActivity.class);
                        //startActivity(i);
                        //finish();
                        break;

                    case "logout":
                        dialogKeluar();
                        break;
                }

                return true;

            }
        });

        result.withSelectedItem(0);

    }
    */

}