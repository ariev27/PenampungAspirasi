package id.dangersstudio.penampungaspirasi.models;

/**
 * Created by ariev27 on 8/27/17.
 */

public class ModelRespon {
    private String id_respon, id_user, id_pengaduan, respon, tgl_respon, foto;

    public String getId_respon() {
        return id_respon;
    }

    public void setId_respon(String id_respon) {
        this.id_respon = id_respon;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getId_pengaduan() {
        return id_pengaduan;
    }

    public void setId_pengaduan(String id_pengaduan) {
        this.id_pengaduan = id_pengaduan;
    }

    public String getRespon() {
        return respon;
    }

    public void setRespon(String respon) {
        this.respon = respon;
    }

    public String getTgl_respon() {
        return tgl_respon;
    }

    public void setTgl_respon(String tgl_respon) {
        this.tgl_respon = tgl_respon;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
}
