package id.dangersstudio.penampungaspirasi.models;

/**
 * Created by ariev27 on 8/22/17.
 */

public class ModelKategori {
    public String idKategori, kategori;

    public String getIdKategori() {
        return idKategori;
    }

    public void setIdKategori(String idKategori) {
        this.idKategori = idKategori;
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }
}
