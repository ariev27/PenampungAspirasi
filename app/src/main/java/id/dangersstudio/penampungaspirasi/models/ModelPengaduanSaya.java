package id.dangersstudio.penampungaspirasi.models;

/**
 * Created by ariev27 on 8/16/17.
 */

public class ModelPengaduanSaya {
    private String id_pengaduan, id_user, id_kategori, pengaduan, tgl_pengaduan, foto;

    public String getId_pengaduan() {
        return id_pengaduan;
    }

    public void setId_pengaduan(String id_pengaduan) {
        this.id_pengaduan = id_pengaduan;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getId_kategori() {
        return id_kategori;
    }

    public void setId_kategori(String id_kategori) {
        this.id_kategori = id_kategori;
    }

    public String getPengaduan() {
        return pengaduan;
    }

    public void setPengaduan(String pengaduan) {
        this.pengaduan = pengaduan;
    }

    public String getTgl_pengaduan() {
        return tgl_pengaduan;
    }

    public void setTgl_pengaduan(String tgl_pengaduan) {
        this.tgl_pengaduan = tgl_pengaduan;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
}
