package id.dangersstudio.penampungaspirasi.activity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import id.dangersstudio.penampungaspirasi.R;
import id.dangersstudio.penampungaspirasi.base.BaseActivity;
import id.dangersstudio.penampungaspirasi.helpers.Constants;
import id.dangersstudio.penampungaspirasi.helpers.RbHelper;
import id.dangersstudio.penampungaspirasi.models.ModelRespon;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static android.view.View.GONE;

// Kelas publik yang meng extends BaseActivity agar dapat
// Menggunakan variabel atau method yang ada di BaseActivity
public class LihatResponActivity extends BaseActivity {

    // Deklarasi variabel
    private RelativeLayout containerRespon;
    private TextView tvIdPengaduan, tvTglPengaduan, tvKategori, tvPengaduan, tvIdRespon, tvTglRespon, tvRespon;
    private String idPengaduan, tglPengaduan, idKategori, pengaduan, fotoPengaduan;
    private ImageView ivPengaduan, ivRespon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lihat_respon);

        idPengaduan = getIntent().getStringExtra(Constants.ID_PENGADUAN);
        tglPengaduan = getIntent().getStringExtra(Constants.TGL_PENGADUAN);
        idKategori = getIntent().getStringExtra(Constants.ID_KATEGORI);
        pengaduan = getIntent().getStringExtra(Constants.PENGADUAN);
        fotoPengaduan = getIntent().getStringExtra(Constants.FOTO_PENGADUAN);

        // Memanggil method setupView dan getRespon
        setupView();
        getRespon();

    }

    // Method untuk mengkoneksikan variabel di xml dengan variabel di activity
    private void setupView() {
        containerRespon = (RelativeLayout) findViewById(R.id.containerRespon);
        tvIdPengaduan = (TextView) findViewById(R.id.tvIdPengaduan);
        tvTglPengaduan = (TextView) findViewById(R.id.tvTglPengaduan);
        tvKategori = (TextView) findViewById(R.id.tvKategori);
        tvPengaduan = (TextView) findViewById(R.id.tvPengaduan);
        tvIdRespon = (TextView) findViewById(R.id.tvIdRespon);
        tvTglRespon = (TextView) findViewById(R.id.tvTglRespon);
        tvRespon = (TextView) findViewById(R.id.tvRespon);
        ivPengaduan = (ImageView) findViewById(R.id.ivPengaduan);
        //ivRespon = (ImageView) findViewById(R.id.ivRespon);

        // Set tvIdPengaduan dengan nilai dari idPengaduan
        tvIdPengaduan.setText(idPengaduan);

        // Set tvTglPengaduan dengan nulai dari tglPengaduan
        tvTglPengaduan.setText(tglPengaduan);

        // Set tvPengaduan dengan nulai dari pengaduan
        tvPengaduan.setText(pengaduan);

        // Jika idKategori 1
        if (idKategori.equals("1")) {

            // Maka set teks tvKategori dengan Keluhan Pelayanan
            tvKategori.setText("Keluhan Pelayanan");
        } else {

            // Maka set teks tvKategori dengan Laporan Keterjadian
            tvKategori.setText("Laporan Keterjadian");
        }

        Glide
                .with(c)

                // Ambil foto dari fotoPengaduan
                .load(fotoPengaduan)

                // Bila sudah di ambil masukan kedalam cache aga irit bandwith atau quota
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                //.bitmapTransform(new CropCircleTransformation(c))
                //.centerCrop()

                // Set foto default dengan ic_profile_default
                .placeholder(R.drawable.ic_profile_default)
                .crossFade()

                // Ganti atau masukan foto yang telah di ambil ke ivPengaduan
                .into(ivPengaduan);
    }

    // Method untuk mengambil respon
    private void getRespon() {

        // Set data yang akan di kirim dengan method GET
        RequestBody formBody = new FormBody.Builder()
                .add("id_pengaduan", idPengaduan)
                //.add("token", sesi.getToken())
                .build();

        // Url untuk GET data
        String url = RbHelper.BASE_URL + "respon.php";

        // GET data yang telah di set
        Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .build();
        RbHelper.pre("url " + url);

        // Munculkan animasi loading dengan memanggil method showLoading
        showLoading();

        // Menangani callback
        client.newCall(request).enqueue(new okhttp3.Callback() {

            // Jika gagal
            @Override
            public void onFailure(Call call, final IOException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        // Hilangkan animasi loading
                        hideLoading();

                        // Munculkan pesan error
                        RbHelper.pesan(c, "Error " + e.getMessage());
                    }
                });
            }


            // Jika ada respon
            @Override
            public void onResponse(Call call, Response response) throws IOException {

                // Jika responnya tidak sukses
                if (!response.isSuccessful()) {

                    // Hilangkan animasi loading
                    hideLoading();

                    // Munculkan pesan error
                    throw new IOException("unexpected code" + response);
                }

                // Inisiasi variabel responData dan rubah ke bentuk string
                final String responData = response.body().string();
                RbHelper.pre("respone " + responData);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {

                            // Hilangkan animasi loading
                            hideLoading();

                            // Inisiasi variabel json dan isi dengan responData
                            JSONObject json = new JSONObject(responData);

                            //Cek status atau result
                            boolean result = json.getBoolean("result");

                            //String msg = json.getString("msg");
                            //RbHelper.pesan(c, msg);

                            // Jika status atau result true
                            if (result) {

                                // Inisiasi variabel jobj dengan string data
                                JSONObject jobj = json.getJSONObject("data");

                                ModelRespon x = new ModelRespon();

                                // Ambil string id_respon dan masukan ke variabel id_respon
                                x.setId_respon(jobj.getString("id_respon"));

                                // Ambil string id_user dan masukan ke variabel id_user
                                x.setId_user(jobj.getString("id_user"));

                                // Ambil string respon dan masukan ke variabel respon
                                x.setRespon(jobj.getString("respon"));

                                // Ambil string tgl_respon dan masukan ke variabel tgl_respon
                                x.setTgl_respon(jobj.getString("tgl_respon"));

                                // Ambil string foto dan masukan ke variabel foto
                                x.setFoto(jobj.getString("foto"));

                                // Set teks tvIdRespon dengan nilai dari id_respon
                                tvIdRespon.setText(x.getId_respon());

                                // Set teks tvTglRespon dengan nilai dari tgl_respon
                                tvTglRespon.setText(x.getTgl_respon());

                                // Set teks tvRespon dengan nilai dari respon
                                tvRespon.setText(x.getRespon());

                                /*
                                Glide
                                        .with(c)
                                        .load(x.getFoto())
                                        .diskCacheStrategy(DiskCacheStrategy.RESULT)
                                        //.bitmapTransform(new CropCircleTransformation(c))
                                        //.centerCrop()
                                        .placeholder(R.drawable.ic_profile_default)
                                        .crossFade()
                                        .into(ivRespon);
                                */

                                // Jika status atau result false
                            } else {

                                // Inisiasi variabel msg dan ambil string msg dari responData
                                String msg = json.getString("msg");

                                // Tampilkan pesan error
                                RbHelper.pesan(c, msg);

                                // Hilangkan containerRespon
                                containerRespon.setVisibility(GONE);
                            }

                        // Jika ada error dalam responData atau parsing data json
                        } catch (JSONException e) {
                            e.printStackTrace();

                            // Munculkan pesan error
                            RbHelper.pesan(c, "Error parsing json");

                        // Jika ada error
                        } catch (Exception e) {
                            e.printStackTrace();

                            // Munculkan pesan error
                            RbHelper.pesan(c, "Login gagal");
                        }
                    }
                });
            }
        });
    }

}
