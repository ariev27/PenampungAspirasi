package id.dangersstudio.penampungaspirasi.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

import id.dangersstudio.penampungaspirasi.R;
import id.dangersstudio.penampungaspirasi.base.BaseActivity;
import id.dangersstudio.penampungaspirasi.helpers.FileUtility;
import id.dangersstudio.penampungaspirasi.helpers.ImageCustomize;
import id.dangersstudio.penampungaspirasi.helpers.RbHelper;
import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

// Kelas publik yang meng extends BaseActivity agar dapat
// Menggunakan variabel atau method yang ada di BaseActivity
public class DaftarActivity extends BaseActivity {

    // Deklarasi variabel
    private ImageView ivProfile, ivCamera;
    private static final int SELECT_PHOTO = 321;
    private boolean isImage = false;
    private String path = "";
    private EditText txtNamaLengkap, txtUsername, txtPassword, txtAlamat, txtNoTelp;
    private Button btnDaftar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar);

        // Memanggil method setupView
        setupView();
    }

    // Method untuk mengkoneksikan variabel di xml dengan variabel di activity
    private void setupView() {
        txtNamaLengkap = (EditText) findViewById(R.id.txtNamaLengkap);
        txtUsername = (EditText) findViewById(R.id.txtUsername);
        txtPassword = (EditText) findViewById(R.id.txtPassword);
        txtAlamat = (EditText) findViewById(R.id.txtAlamat);
        txtNoTelp = (EditText) findViewById(R.id.txtNoTelp);

        ivProfile = (ImageView) findViewById(R.id.ivProfile);

        btnDaftar = (Button) findViewById(R.id.btnDaftar);
        btnDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Animasi klik
                view.startAnimation(btnAnimasi);

                // Panggil method actionDaftar
                actionDaftar();
            }
        });

        ivCamera = (ImageView) findViewById(R.id.ivCamera);

        // Bila ivCamera di klik akan berpindah ke MenuUtamaActivity menggunakan Intent
        ivCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Animasi klik
                view.startAnimation(btnAnimasi);

                //Tampilkan pilihan untuk memilih foto dari galeri atau kamera
                callGaleri();
            }
        });
    }

    // Method callGaleri
    private void callGaleri() {
        Intent intent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivityForResult(intent, SELECT_PHOTO);
    }


    //Untuk handle gambar yang di pilih
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case SELECT_PHOTO:
                if (resultCode == Activity.RESULT_OK) {

                    // Inisiasi image terpilih ke variabel selectedImage
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getContentResolver().query(selectedImage,
                            filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    // Ambil path image yang terpilih
                    int index = cursor.getColumnIndex(filePathColumn[0]);
                    String pictPath = cursor.getString(index);
                    cursor.close();

                    // Membuat file/ image baru dengan source pictPath
                    File f = new File(pictPath);

                    // Resize image, compress, dan convert ke format .png
                    // Menggunakan ImageCustomize yang telah di buat sebelumnya
                    File file1 = ImageCustomize.resizeImage(f);
                    Bitmap bmp = ImageCustomize.decodeFile(file1, 300);

                    // Set ivProfile dengan bmp
                    ivProfile.setImageBitmap(bmp);

                    // Mengambil path dari file1
                    path = file1.getAbsolutePath();

                    // Set variabel isImage dengan true
                    isImage = true;
                } else {

                    // Set variabel isImage dengan false
                    isImage = false;
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    // Method actionDaftar
    private void actionDaftar() {
        txtNamaLengkap.setError(null);
        txtUsername.setError(null);
        txtPassword.setError(null);
        txtAlamat.setError(null);
        txtNoTelp.setError(null);

        boolean cancel = false;
        View focusView = null;

        // Jika bukan image
        if (!isImage) {

            // Maka tampilkan pesan error dengan Toast
            Toast.makeText(c, "Silahkan pilih foto profil", Toast.LENGTH_SHORT).show();

            // Pindahkan fokus ke ivProfile
            focusView = ivProfile;
            cancel = true;
        } else if (RbHelper.isEmpty(txtNamaLengkap)) {

            // Set pesan error
            txtNamaLengkap.setError("Nama Lengkap harus di isi !!");

            // Pindahkan fokus ke txtNamaLengkap
            focusView = txtNamaLengkap;
            cancel = true;
        } else if (RbHelper.isEmpty(txtUsername)) {

            // Set pesan error
            txtUsername.setError("Username harus di isi !!");

            // Pindahkan fokus ke txtUsername
            focusView = txtUsername;
            cancel = true;
        } else if (RbHelper.isEmpty(txtPassword)) {

            // Set pesan error
            txtPassword.setError("Password harus di isi !!");

            // Pindahkan fokus ke txtPassword
            focusView = txtPassword;
            cancel = true;
        } else if (RbHelper.isEmpty(txtAlamat)) {

            // Set pesan error
            txtAlamat.setError("Alamat harus di isi !!");

            // Pindahkan fokus ke txtPassword
            focusView = txtPassword;
            cancel = true;
        } else if (RbHelper.isEmpty(txtNoTelp)) {

            // Set pesan error
            txtNoTelp.setError("No Telpon/ HP harus di isi !!");

            // Pindahkan fokus ke txtNoTelp
            focusView = txtNoTelp;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {

            // Set data yang akan di kirim dengan method POST
            RequestBody formBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("username", txtUsername.getText().toString())
                    .addFormDataPart("password", txtPassword.getText().toString())
                    .addFormDataPart("nama_lengkap", txtNamaLengkap.getText().toString())
                    .addFormDataPart("alamat", txtAlamat.getText().toString())
                    .addFormDataPart("no_telp", txtNoTelp.getText().toString())
                    .addFormDataPart("foto", "foto_profile.png",
                            RequestBody.create(MediaType.parse("image/png"), new File(path)))
                    .build();

            // Url untuk POST data
            String url = RbHelper.BASE_URL + "register.php";

            // POST data yang telah di set
            Request request = new Request.Builder()
                    .url(url)
                    .post(formBody)
                    .build();
            RbHelper.pre("url " + url);

            // Munculkan animasi loading dengan memanggil method showLoading
            showLoading();

            // Menangani callback
            client.newCall(request).enqueue(new okhttp3.Callback() {

                // Jika gagal
                @Override
                public void onFailure(Call call, final IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            // Hilangkan animasi loading
                            hideLoading();

                            // Munculkan pesan error
                            RbHelper.pesan(c, "Error " + e.getMessage());
                        }
                    });
                }


                // Jika ada respon
                @Override
                public void onResponse(Call call, Response response) throws IOException {

                    // Jika responnya tidak sukses
                    if (!response.isSuccessful()) {

                        // Hilangkan animasi loading
                        hideLoading();

                        // Munculkan pesan error
                        throw new IOException("unexpected code" + response);
                    }

                    // Inisiasi variabel responData dan rubah ke bentuk string
                    final String responData = response.body().string();
                    RbHelper.pre("respone " + responData);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {

                                // Hilangkan animasi loading
                                hideLoading();

                                // Inisiasi variabel json dan isi dengan responData
                                JSONObject json = new JSONObject(responData);

                                //Cek status
                                //boolean hasil = json.getBoolean("status");

                                // Ambil data json dengan string msg
                                String msg = json.getString("msg");

                                // Tampilkan pesan atau statusnya
                                RbHelper.pesan(c, msg);
                                startActivity(new Intent(c, LoginActivity.class));

                            // Jika ada error dalam responData atau parsing data json
                            } catch (JSONException e) {
                                e.printStackTrace();

                                // Munculkan pesan error
                                RbHelper.pesan(c, "Error parsing json");

                            // Jika ada error
                            } catch (Exception e) {
                                e.printStackTrace();

                                // Munculkan pesan error
                                RbHelper.pesan(c, "Pendaftaran gagal");
                            }
                        }
                    });
                }
            });
        }
    }


}
