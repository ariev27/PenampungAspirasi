package id.dangersstudio.penampungaspirasi.activity;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.IdRes;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import id.dangersstudio.penampungaspirasi.R;
import id.dangersstudio.penampungaspirasi.base.BaseActivity;
import id.dangersstudio.penampungaspirasi.helpers.ImageCustomize;
import id.dangersstudio.penampungaspirasi.helpers.RbHelper;
import id.dangersstudio.penampungaspirasi.helpers.SessionManager;
import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

// Kelas publik yang meng extends BaseActivity agar dapat
// Menggunakan variabel atau method yang ada di BaseActivity
public class BuatPengaduanActivity extends BaseActivity {

    // Deklarasi variabel
    private ImageView ivPengaduan;
    private EditText txtPengaduan;
    private Button btnKirim, btnBatal;
    private RadioGroup radioKategori;
    private String idPengaduan = "", idKategori = "1", tgl;

    private static final int SELECT_PHOTO = 321;
    private boolean isImage = false;
    private String path = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buat_pengaduan);

        // Memanggil method setupView
        setupView();
    }

    // Method untuk mengkoneksikan variabel di xml dengan variabel di activity
    private void setupView() {
        ivPengaduan = (ImageView) findViewById(R.id.ivPengaduan);
        txtPengaduan = (EditText) findViewById(R.id.txtPengaduan);
        btnKirim = (Button) findViewById(R.id.btnKirim);
        btnBatal = (Button) findViewById(R.id.btnBatal);
        radioKategori = (RadioGroup) findViewById(R.id.radioKategori);

        // Bila btnBatal di klik akan berpindah ke MenuUtamaActivity menggunakan Intent
        btnBatal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Animasi klik
                view.startAnimation(btnAnimasi);

                // Arahkan ke MenuUtamaActivity
                startActivity(new Intent(c, MenuUtamaActivity.class));
            }
        });

        // Bila ivPengaduan di klik akan berpindah ke gallery untuk memilih foto
        ivPengaduan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Animasi klik
                view.startAnimation(btnAnimasi);

                // Panggil method callGaleri
                callGaleri();
            }
        });

        radioKategori.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {

                // Jika radioKeluhanPelayanan yang di ceklis maka isi variabel idKategori dengan nilai 1
                // Apabila bukan, maka isi dengan nilai 2
                if (i == R.id.radioKeluhanPelayanan) {
                    idKategori = "1";
                } else {
                    idKategori = "2";
                }
            }
        });

        // Bila di klik akan mengirim pengaduan
        btnKirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Animasi klik
                view.startAnimation(btnAnimasi);

                // Panggil method kirimPengaduan
                kirimPengaduan();
            }
        });
    }

    // Method callGaleri
    private void callGaleri() {
        Intent intent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivityForResult(intent, SELECT_PHOTO);
    }


    // Untuk handle gambar yang di pilih
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case SELECT_PHOTO:
                if (resultCode == Activity.RESULT_OK) {

                    // Inisiasi image terpilih ke variabel selectedImage
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getContentResolver().query(selectedImage,
                            filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    // Ambil path image yang terpilih
                    int index = cursor.getColumnIndex(filePathColumn[0]);
                    String pictPath = cursor.getString(index);
                    cursor.close();

                    // Membuat file/ image baru dengan source pictPath
                    File f = new File(pictPath);

                    // Resize image, compress, dan convert ke format .png
                    // Menggunakan ImageCustomize yang telah di buat sebelumnya
                    File file1 = ImageCustomize.resizeImage(f);
                    Bitmap bmp = ImageCustomize.decodeFile(file1, 300);

                    // Set ivPengaduan dengan bmp
                    ivPengaduan.setImageBitmap(bmp);

                    // Mengambil path dari file1
                    path = file1.getAbsolutePath();

                    // Set variabel isImage dengan true
                    isImage = true;
                } else {

                    // Set variabel isImage dengan false
                    isImage = false;
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    // Method mengirim pengaduan
    private void kirimPengaduan() {

        // Inisiasi variabel sesi
        SessionManager sesi = new SessionManager(getApplicationContext());
        txtPengaduan.setError(null);

        // Inisiasi variabel tgl dan isi dengan tanggal sekarang
        // Dengan format tahun, bulan, hari, menit, detik
        tgl = new SimpleDateFormat("yyyy-MM-dd-mm-ss").format(new Date());
        idPengaduan = idKategori + "-" + tgl;

        boolean cancel = false;
        View focusView = null;

        // Jika nilai isImage = false
        if (!isImage) {

            // Munculkan pesan atau pemberitahuan menggunakan toast
            RbHelper.pesan(c, "Silahkan pilih foto pengaduan dahulu");

            // Pindahkan fokus ke ivPengaduan
            focusView = ivPengaduan;
            cancel = true;

        // Jika txtPengaduan tidak di isi atau kosong
        } else if (RbHelper.isEmpty(txtPengaduan)) {

            // Munculkan pesan atau pemberitahuan menggunakan toast
            txtPengaduan.setError("Pengaduan harus di isi !");

            // Pindahkan fokus ke txtPengaduan
            focusView = txtPengaduan;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {

            // Set data yang akan di kirim dengan method POST
            RequestBody formBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("id_pengaduan", idPengaduan)
                    .addFormDataPart("id_user", sesi.getIDUser())
                    .addFormDataPart("id_kategori", idKategori)
                    .addFormDataPart("pengaduan", txtPengaduan.getText().toString())
                    .addFormDataPart("tgl_pengaduan", tgl)
                    .addFormDataPart("foto", "foto_profile.png",
                            RequestBody.create(MediaType.parse("image/png"), new File(path)))
                    .build();

            // Url untuk POST data
            String url = RbHelper.BASE_URL + "buat-pengaduan.php";

            // POST data yang telah di set
            Request request = new Request.Builder()
                    .url(url)
                    .post(formBody)
                    .build();
            RbHelper.pre("url " + url);

            // Munculkan animasi loading dengan memanggil method showLoading
            showLoading();

            // Menangani callback
            client.newCall(request).enqueue(new okhttp3.Callback() {

                // Jika gagal
                @Override
                public void onFailure(Call call, final IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            // Hilangkan animasi loading
                            hideLoading();

                            // Munculkan pesan error
                            RbHelper.pesan(c, "Error " + e.getMessage());
                        }
                    });
                }

                // Jika ada respon
                @Override
                public void onResponse(Call call, Response response) throws IOException {

                    // Jika responnya tidak sukses
                    if (!response.isSuccessful()) {

                        // Hilangkan animasi loading
                        hideLoading();

                        // Munculkan pesan error
                        throw new IOException("unexpected code" + response);
                    }

                    // Inisiasi variabel responData dan rubah ke bentuk string
                    final String responData = response.body().string();
                    RbHelper.pre("respone " + responData);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {

                                // Hilangkan animasi loading
                                hideLoading();

                                // Inisiasi variabel json dan isi dengan responData
                                JSONObject json = new JSONObject(responData);

                                //Cek status
                                //boolean hasil = json.getBoolean("status");

                                // Ambil data json dengan string msg
                                String msg = json.getString("msg");

                                // Tampilkan pesan atau statusnya
                                RbHelper.pesan(c, msg);

                                // Arahkan ke MenuUtamaActivity
                                startActivity(new Intent(c, MenuUtamaActivity.class));
                                finish();

                            // Jika ada error dalam responData atau parsing data json
                            } catch (JSONException e) {
                                e.printStackTrace();

                                // Munculkan pesan error
                                RbHelper.pesan(c, "Error parsing json");

                            // Jika ada error
                            } catch (Exception e) {
                                e.printStackTrace();

                                // Munculkan pesan error
                                RbHelper.pesan(c, "Pengaduan gagal dikirim");
                            }
                        }
                    });
                }
            });
        }
    }
}
