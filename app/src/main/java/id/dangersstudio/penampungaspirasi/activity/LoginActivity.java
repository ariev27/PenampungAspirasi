package id.dangersstudio.penampungaspirasi.activity;

/**
 * Created by Ariev27
 * Ariev27@live.com
 */

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import id.dangersstudio.penampungaspirasi.R;
import id.dangersstudio.penampungaspirasi.base.BaseActivity;
import id.dangersstudio.penampungaspirasi.helpers.RbHelper;
import id.dangersstudio.penampungaspirasi.helpers.SessionManager;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

// Kelas publik yang meng extends BaseActivity agar dapat
// Menggunakan variabel atau method yang ada di BaseActivity
public class LoginActivity extends BaseActivity {

    // Deklarasi variabel
    private EditText txtUsername, txtPassword;
    private Button btnLogin, btnDaftar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Memanggil method setupView dan cekLogin
        setupView();
        cekLogin();

    }

    // Method untuk mengecek apakah user telah login atau tidak
    // Jika ya akan dialihkan ke MenuUtamaActivity
    // Dan apabila tidak akan di alihkan ke LoginActivity
    private void cekLogin() {
        SessionManager sesi = new SessionManager(getApplicationContext());

        if (sesi.isLoggedIn()) {
            Intent intent = new Intent(getApplicationContext(),
                    MenuUtamaActivity.class);
            startActivity(intent);
            finish();
        }
    }

    // Method untuk mengkoneksikan variabel di xml dengan variabel di activity
    private void setupView() {
        txtUsername = (EditText) findViewById(R.id.txtUsername);
        txtPassword = (EditText) findViewById(R.id.txtPassword);
        btnDaftar = (Button) findViewById(R.id.btnDaftar);
        btnLogin = (Button) findViewById(R.id.btnLogin);

        // Bila btnLogin di klik akan melakukan prosedur login
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Animasi klik
                view.startAnimation(btnAnimasi);

                // Panggil method actionLogin
                actionLogin();
            }
        });

        btnDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Animasi klik
                view.startAnimation(btnAnimasi);

                // Panggil method actionDaftar
                actionDaftar();
            }
        });
    }

    // Method untuk login
    private void actionLogin() {
        txtUsername.setError(null);
        txtPassword.setError(null);

        boolean cancel = false;
        View focusView = null;

        // Ketika txtUsername atau txtPassword tidak di isi atau kosong
        // Maka akan menampilkan pesan error
        if (RbHelper.isEmpty(txtUsername)) {
            txtUsername.setError("Username harus di isi !!");
            focusView = txtUsername;
            cancel = true;
        } else if (RbHelper.isEmpty(txtPassword)) {
            txtPassword.setError("Password harus di isi !!");
            focusView = txtPassword;
            cancel = true;
        }
        if (cancel) {
            focusView.requestFocus();
        } else {

            // Set data yang akan di kirim dengan method POST
            RequestBody formBody = new FormBody.Builder()
                    .add("username", txtUsername.getText().toString())
                    .add("password", txtPassword.getText().toString())
                    .build();

            // Url untuk POST data
            String url = RbHelper.BASE_URL + "login.php";

            // POST data yang telah di set
            Request request = new Request.Builder()
                    .url(url)
                    .post(formBody)
                    .build();
            RbHelper.pre("url " + url);

            // Munculkan animasi loading dengan memanggil method showLoading
            showLoading();

            // Menangani callback
            client.newCall(request).enqueue(new okhttp3.Callback() {

                // Jika gagal
                @Override
                public void onFailure(Call call, final IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            // Hilangkan animasi loading
                            hideLoading();

                            // Munculkan pesan error
                            RbHelper.pesan(c, "Error " + e.getMessage());
                        }
                    });
                }

                // Jika ada respon
                @Override
                public void onResponse(Call call, Response response) throws IOException {

                    // Jika responnya tidak sukses
                    if (!response.isSuccessful()) {

                        // Hilangkan animasi loading
                        hideLoading();

                        // Munculkan pesan error
                        throw new IOException("unexpected code" + response);
                    }

                    // Inisiasi variabel responData dan rubah ke bentuk string
                    final String responData = response.body().string();
                    RbHelper.pre("respone " + responData);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {

                                // Hilangkan animasi loading
                                hideLoading();

                                // Inisiasi variabel json dan isi dengan responData
                                JSONObject json = new JSONObject(responData);

                                //Cek status
                                boolean result = json.getBoolean("result");

                                // Ambil data json dengan string msg
                                String msg = json.getString("msg");

                                // Tampilkan pesan atau statusnya
                                RbHelper.pesan(c, msg);

                                // Jika status atau result true
                                if (result) {
                                    //Buat sesi loginnya
                                    //String token = json.getString("token");
                                    //Ambil data user

                                    // Inisiasi variabel jobj dengan string data
                                    JSONObject jObj = json.getJSONObject("data");

                                    // Ambil data json, buat variabel, dan
                                    // Masukan data json tersebut kedalam variabel
                                    String idUser = jObj.getString("id_user");
                                    String level = jObj.getString("level");
                                    String username = jObj.getString("username");
                                    String token = jObj.getString("token");
                                    String nama = jObj.getString("nama_lengkap");
                                    String alamat = jObj.getString("alamat");
                                    String noTelp = jObj.getString("no_telp");
                                    String foto = jObj.getString("foto");

                                    //Masukan kedalam session manager
                                    SessionManager sesi = new SessionManager(c);
                                    sesi.createLoginSession(username);
                                    sesi.setIDUser(idUser);
                                    sesi.setLevel(level);
                                    sesi.setUsername(username);
                                    sesi.setToken(token);
                                    sesi.setNamaLengkap(nama);
                                    sesi.setAlamat(alamat);
                                    sesi.setNotelp(noTelp);
                                    sesi.setFoto(foto);

                                    //Arahkan ke halaman main
                                    startActivity(new Intent(c, MenuUtamaActivity.class));
                                    finish();
                                }

                            // Jika ada error dalam responData atau parsing data json
                            } catch (JSONException e) {
                                e.printStackTrace();

                                // Munculkan pesan error
                                RbHelper.pesan(c, "Error parsing json");

                            // Jika ada error
                            } catch (Exception e) {
                                e.printStackTrace();

                                // Munculkan pesan error
                                RbHelper.pesan(c, "Login gagal");
                            }
                        }
                    });
                }
            });
        }
    }

    // Method untuk mengarahkan ke DaftarActivity
    private void actionDaftar() {
        startActivity(new Intent(c, DaftarActivity.class));
    }

}