package id.dangersstudio.penampungaspirasi.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.github.clans.fab.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import id.dangersstudio.penampungaspirasi.R;
import id.dangersstudio.penampungaspirasi.adapters.PengaduanSayaAdapter;
import id.dangersstudio.penampungaspirasi.base.BaseActivity;
import id.dangersstudio.penampungaspirasi.helpers.RbHelper;
import id.dangersstudio.penampungaspirasi.helpers.SessionManager;
import id.dangersstudio.penampungaspirasi.models.ModelPengaduanSaya;
import jp.wasabeef.glide.transformations.CropCircleTransformation;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

// Kelas publik yang meng extends BaseActivity agar dapat
// Menggunakan variabel atau method yang ada di BaseActivity
public class MenuUtamaActivity extends BaseActivity {

    // Deklarasi variabel
    private ImageView ivProfile;
    private TextView tvNamaLengkap, tvNoTelp;
    private Button btnLogout;
    private FloatingActionButton fabPengaduanBaru;
    private RecyclerView rvPengaduanSaya;
    private ArrayList<ModelPengaduanSaya> pengaduanSaya;
    private PengaduanSayaAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_utama);

        // Memanggil method setupView dan getPengaduanSaya
        setupView();
        getPengaduanSaya();

    }

    // Method untuk mengkoneksikan variabel di xml dengan variabel di activity
    private void setupView() {
        final SessionManager sesi = new SessionManager(getApplicationContext());

        ivProfile = (ImageView) findViewById(R.id.ivProfile);
        tvNamaLengkap = (TextView) findViewById(R.id.tvNamaLengkap);
        tvNoTelp = (TextView) findViewById(R.id.tvNoTelp);
        fabPengaduanBaru = (FloatingActionButton) findViewById(R.id.fabPengaduanBaru);
        rvPengaduanSaya = (RecyclerView) findViewById(R.id.rvPengaduanSaya);
        pengaduanSaya = new ArrayList<>();
        btnLogout = (Button) findViewById(R.id.btnLogout);

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Animasi klik
                view.startAnimation(btnAnimasi);

                // Panggil dialogKeluar
                dialogKeluar();
            }
        });

        rvPengaduanSaya.setLayoutManager(new LinearLayoutManager(c));

        fabPengaduanBaru.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Animasi klik
                view.startAnimation(btnAnimasi);

                // Munculkan animasi loading
                showLoading();

                // Alihkan ke BuatPengaduanActivity menggunakan intent
                startActivity(new Intent(c, BuatPengaduanActivity.class));
                finish();

                // Hilangkan animasi loading
                hideLoading();
            }
        });

        // Menggunakan library glide untuk mengambil image dari url
        // Dan menerapkan ke ivProfile
        Glide
                .with(c)

                // Load url image
                .load(sesi.getFoto())

                // Image yg telah di ambil masukan ke cache agar
                // Loading lebih cepat dan irit bandwith
                .diskCacheStrategy(DiskCacheStrategy.ALL)

                // Rubah bentuk image ke bulat
                .bitmapTransform(new CropCircleTransformation(c))
                //.centerCrop()

                // Gambar default jika tidak ada image , broken link, error, dll
                .placeholder(R.drawable.ic_profile_default)
                .crossFade()
                .into(ivProfile);

        // Set tvNamaLengkap, tvNoTelp dengan data yang sebelumnya
        // Telah kita simpan di SessionManager
        tvNamaLengkap.setText(sesi.getNamaLengkap());
        tvNoTelp.setText(sesi.getNotelp());
    }

    // Method untuk mengambil pengaduan yang telah di kirim
    private void getPengaduanSaya() {
        SessionManager sesi = new SessionManager(getApplicationContext());
        pengaduanSaya.clear();

        // Set data yang akan di kirim dengan method POST
        RequestBody formBody = new FormBody.Builder()
                .add("id_user", sesi.getIDUser())
                //.add("token", sesi.getToken())
                .build();

        // Url untuk POST data
        String url = RbHelper.BASE_URL + "pengaduan-saya.php";

        // POST data yang telah di set
        Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .build();
        RbHelper.pre("url " + url);

        // Munculkan animasi loading dengan memanggil method showLoading
        showLoading();

        // Menangani callback
        client.newCall(request).enqueue(new okhttp3.Callback() {

            // Jika gagal
            @Override
            public void onFailure(Call call, final IOException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        // Hilangkan animasi loading
                        hideLoading();

                        // Munculkan pesan error
                        RbHelper.pesan(c, "Error " + e.getMessage());
                    }
                });
            }

            // Jika ada respon
            @Override
            public void onResponse(Call call, Response response) throws IOException {

                // Jika responnya tidak sukses
                if (!response.isSuccessful()) {

                    // Hilangkan animasi loading
                    hideLoading();

                    // Munculkan pesan error
                    throw new IOException("unexpected code" + response);
                }

                // Inisiasi variabel responData dan rubah ke bentuk string
                final String responData = response.body().string();
                RbHelper.pre("respone " + responData);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {

                            // Hilangkan animasi loading
                            hideLoading();

                            // Inisiasi variabel json dan isi dengan responData
                            JSONObject json = new JSONObject(responData);

                            //Cek status
                            boolean result = json.getBoolean("result");

                            //String msg = json.getString("msg");
                            //RbHelper.pesan(c, msg);

                            // Jika status atau result true akan melakukan looping
                            // Untuk mengambil semua data json karena berbentuk json array bukan object
                            if (result) {
                                JSONArray jsonArray = json.getJSONArray("item");
                                for (int i = 0; i < jsonArray.length(); i++) {

                                    JSONObject jobj = jsonArray.getJSONObject(i);
                                    ModelPengaduanSaya x = new ModelPengaduanSaya();

                                    x.setId_pengaduan(jobj.getString("id_pengaduan"));
                                    x.setId_user(jobj.getString("id_user"));
                                    x.setId_kategori(jobj.getString("id_kategori"));
                                    x.setPengaduan(jobj.getString("pengaduan"));
                                    x.setTgl_pengaduan(jobj.getString("tgl_pengaduan"));
                                    x.setFoto(jobj.getString("foto"));

                                    // Masukan data ke dalam array list
                                    pengaduanSaya.add(x);
                                }

                                // Buat adapter baru
                                adapter = new PengaduanSayaAdapter(c, pengaduanSaya);

                                // set recycler view dengan adapter yg telah di buat
                                rvPengaduanSaya.setAdapter(adapter);

                            // Jika result false tampilkan pesan error
                            } else {
                                String msg = json.getString("msg");
                                RbHelper.pesan(c, msg);
                            }

                        // Jika ada error dalam responData atau parsing data json
                        } catch (JSONException e) {
                            e.printStackTrace();

                            // Munculkan pesan error
                            RbHelper.pesan(c, "Error parsing json");

                        // Jika ada error
                        } catch (Exception e) {
                            e.printStackTrace();

                            // Munculkan pesan error
                            RbHelper.pesan(c, "Login gagal");
                        }
                    }
                });
            }
        });
    }

}
